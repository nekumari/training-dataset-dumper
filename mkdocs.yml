site_name: FTAG Dumpster documentation
site_description: Documentation for the ATLAS FTAG dataset dumper
site_author: training-dataset-dumper team
site_url: https://training-dataset-dumper.docs.cern.ch/
copyright: Copyright &copy; 2002 - 2024 CERN for the benefit of the ATLAS collaboration

repo_name: GitLab
repo_url: https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/
edit_uri: tree/main/docs

theme:
  name: material
  logo: assets/dumpster.png
  favicon: assets/dumpster.png
  features:
    - navigation.instant
    - navigation.tracking
    - navigation.sections
    - navigation.indexes
    - navigation.top
    - content.code.copy
    - content.action.edit

nav:
  - index.md

  - Usage:
    - Installation: installation.md
    - Local Dumps: local.md
    - Grid Dumps: grid.md
    - Configuration: configuration.md
    - Advanced Features: advanced_features.md
    - CA Configuration: ca_blocks.md

  - Development:
      - Contributing: contributing.md
      - Adding CI Tests: tests.md

  - Outputs:
      - Tools & Defaults: outputs.md
      - PFlow Jet Outputs: vars_pflow.md
      - Open Dataset Outputs: vars_open.md


  - Tutorials:
    - Dataset dumper tutorial: tutorial-tdd.md
    - Athena algorithms tutorial: tutorial-algorithms.md

plugins:
  - search
  - mermaid2
  - markdownextradata
  - autorefs
  - git-revision-date-localized:
      enable_creation_date: true
      type: date
  - mkdocstrings:
      handlers:
        python:
          options:
            line_length: 100
            show_root_heading: true
            show_root_full_path: false
            docstring_style: numpy
            merge_init_into_class: false
            annotations_path: full
            show_signature: true


markdown_extensions:
  - admonition
  - codehilite
  - pymdownx.arithmatex
  - pymdownx.details
  - pymdownx.inlinehilite
  - pymdownx.smartsymbols
  - pymdownx.highlight
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - toc:
      permalink: "#"
  - footnotes
  - pymdownx.magiclink:
      user: atlas-flavor-tagging-tools
      repo: training-dataset-dumper
      repo_url_shorthand: true
      provider: gitlab
      custom:
        gitlab:
          host: gitlab.cern.ch
          label: gitlab
          type: gitlab
  - attr_list

extra_javascript:
    - 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML'
