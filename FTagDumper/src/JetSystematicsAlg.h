// CP interface includes
#include "PATInterfaces/SystematicSet.h"
// external tools include(s):
#include "xAODJet/JetContainer.h"
#include "AthenaBaseComps/AthReentrantAlgorithm.h"
// #include "GaudiKernel/ToolHandle.h"
#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
#include "JetCalibTools/IJetCalibrationTool.h"

#include <mutex>

class JetSystematicsAlg :  public AthReentrantAlgorithm {
public:
  JetSystematicsAlg(const std::string& name, ISvcLocator *pSvcLocator);

  virtual ~JetSystematicsAlg();

  /** Main routines specific to an ATHENA algorithm */
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext&) const override;
  virtual StatusCode finalize() override;

private:
  /// @brief set to true if systematics asked for and exist

  CP::SystematicSet m_ActiveSysts;
  SG::ReadHandleKey<xAOD::JetContainer> m_inputContainer {
    this, "jet_collection", "", "input jet collection"
  };
  std::string m_jet_collection;
  std::vector< std::string > m_SystNames;
  // tools
  ToolHandle<IJetCalibrationTool>        m_calibration_tool;
  float m_Sigma = 1.0;
  SG::WriteHandleKey<xAOD::JetContainer>  m_outContainerKey {
    this, "output_jet_collection", "", "output jet collection"
  };

  // Terrible. Making this mutable because the tool requires some
  // state to set the systematic uncertainty being applied
  mutable ToolHandle<ICPJetUncertaintiesTool>    m_uncertainties_tool;
  // protect tool access with a mutex, to make the above marginally
  // less terrible.
  mutable std::mutex m_uncertainty_lock;

};
// class JetSystematicsAlg
