#!/usr/bin/env python3

"""

This script is similar to dump-single-btag, adds TrackFlowOverlapRemovalAlg
Necessary to dump the flow constiturnts and not duplicate the charged ones that are listed in tracks 

"""

import sys
from FTagDumper import dumper

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory


def orCfg(name, jets, tracks='BTagTrackToJetAssociator'):
    ca = ComponentAccumulator()
    ca.addEventAlgo(
        CompFactory.TrackFlowOverlapRemovalAlg(
            name=name,
            Tracks=f'{jets}.{tracks}',
            Constituents=f'{jets}.constituentLinks',
            OutTracks=f'{jets}.nonConstituentTracks'
        )
    )
    ca.addEventAlgo(
        CompFactory.FlowSelectorAlg(
            name="NeutralFlowSelectorAlg",
            Constituents=f'{jets}.constituentLinks',
            OutConstituentsNeutral=f'{jets}.neutralConstituentLinks',
            OutConstituentsCharged=f'{jets}.chargedConstituentLinks'
        )
    )
    return ca


def run():

    args = dumper.base_parser(__doc__).parse_args()

    flags = dumper.update_flags(args)
    flags.lock()

    ca = dumper.getMainConfig(flags)

    ca.merge(orCfg('nonConstituentAdder', 'AntiKt4EMPFlowJets', 'TracksForBTagging'))

    ca.merge(dumper.getDumperConfig(args))
    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)

