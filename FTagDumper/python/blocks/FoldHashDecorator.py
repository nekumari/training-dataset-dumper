from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from FlavorTagDiscriminants.FoldDecoratorConfig import FoldDecoratorCfg

from .BaseBlock import BaseBlock

@dataclass
class FoldHashDecorator(BaseBlock):
    """Decorate jets with a hash for kfold training and evaluation.

    See [FoldDecoratorAlg.cxx](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/src/FoldDecoratorAlg.cxx)
    and [FoldDecoratorConfig.py](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/python/FoldDecoratorConfig.py)

    Parameters
    ----------
    jet_collection : str, optional
        Name of the jet collection to decorate. Use the dumper's jet_collection
        if not provided.
    prefix : str, optional
        Prefix to add to the hash.
    """
    jet_collection: str = None
    prefix: str = ''

    def __post_init__(self):
        if self.jet_collection is None:
            self.jet_collection = self.dumper_config['jet_collection']

    def to_ca(self):
        ca = ComponentAccumulator()
        ca.merge(FoldDecoratorCfg(self.flags, jetCollection=self.jet_collection, prefix=self.prefix))
        return ca
